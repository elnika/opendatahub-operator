# Open Data Hub Examples
Example tutorials, code and notebooks for the Open Data Hub.

Table of Contents:

| Example | Summary | Type |
|--|--|--|
| [basic_tutorial.md](basic_tutorial.md) | A starter tutorial that shows the basics of uploading data to a Ceph data lake and running a sentiment analysis example using JupyterHub, Jupyter notebooks and Spark. | Tutorial |
| [basic_tutorial.ipynb](basic_tutorial.ipynb) | The accompanying notebook for the [basic tutorial](basic_tutorial.md). | Jupyter Notebook |
| [text_analysis.ipynb](text_analysis.ipynb) | A sample Jupyter notebook for analyzing and visualizing data from a tab-separated text file using Spark, numpy, pandas and more. | Jupyter Notebook |
| [neural_network_raw.ipynb](neural_network_raw.ipynb) | A sample Tensorflow neural network Jupyter notebook. | Jupyter Notebook |
